const PROXY_CONFIG = [
  {
    context: ['/Token'],
    target: 'http://192.168.11.32:8092',
    //target: 'http://192.168.37.14:8181',
    // target: 'http://localhost:8091',
    //target: 'http://localhost:8888',
    secure: false,
    logLevel: 'debug',
    changeOrigin: true,
    pathRewrite: {
      '^/': ''
    },
    bypass: function(proxyRes, req, res) {
      // proxyRes.headers['x-msisdn'] = '976358660';
      // proxyRes.headers['x-uli'] = '0647f450299ea15f47f450299e79ff';
    }
  },
  {
    context: ['/MapSifen'],
    target: 'http://192.168.11.32:8092',
    //target: 'http://localhost:8091',
    //target: 'http://localhost:8888',
    secure: false,
    logLevel: 'debug',
    changeOrigin: true,
    pathRewrite: {
      '^/': ''
    },
    bypass: function(proxyRes, req, res) {
      // proxyRes.headers['x-msisdn'] = '976358660';
      // proxyRes.headers['x-uli'] = '0647f450299ea15f47f450299e79ff';
    }
  }
];

module.exports = PROXY_CONFIG;

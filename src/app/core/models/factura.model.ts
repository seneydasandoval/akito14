export interface Factura {
  tipoDocumento: string;
  numeroDocumento: string;
  tipoDocumentoCliente: string;
  codigoDocumentoCliente: string;
  nombreCliente: string;
  fechaEmisionFactura: string;
  fechaEnvioSifen: string;
  fechaFirma: string;
  moneda: string;
  importeTotal: string;
  motivoRechazo: string;
  tipoGestion: string;
  numeroResgisto: string;
}
export interface FacturaDetalle {
  id?: number;
  codigoRamo: number;
  numeroPoliza: string;
  numeroSuplemento: string;
  fechaEmisionPoliza: string;
  codigoUsuario: string;
  fechaLibro: string;
  descripcionMovimiento: string;
  tipoDocumentoModificado: string;
  numeroDocumentoModificado: string;
  conceptoVenta: string;
  estado: string;
  fechaAnulacion: string;
  usuarioAnulacion: string;
  valorCambio: string;
  importeExento: string;
  importeBase: string;
  importeIntereses: string;
  importeRecargo: string;
  importeCuota: string;
  comentario: string;
  usuarioComentario: string;
  fechaComentario: string;
}
export interface Comentario {
  tipoDocumento: string;
  numeroDocumento: string;
  tipoDocumentoCliente: string;
  codigoDocumentoCliente: string;
  fechaEmisionFactura: string;
  codigoTipoCorreccion: string;
  fechaUltimaActualizacion: string;
  codigoUsuario: string;
  numeroMovimiento: string;
  observacion: string;
  codigoEstado: string;
}

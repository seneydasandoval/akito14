import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Servers } from 'src/app/config/api';
import { Factura } from '../models/factura.model';

@Injectable({
  providedIn: 'root'
})
export class FacturasService {
  private data: any;


  private url = Servers.SIFEN.baseUrl + '/gestionFactura';
  private listado='/listaDetallePg';
  private detalle='/facturaDetalle';
  constructor(private http: HttpClient) {}

  getFacturas(params) {

    return this.http.get(this.url + this.listado +"?"+ params, );
  }
  getFactura(params) {
    return this.http.get(this.url + this.detalle + params, );
  }
  getTipoDocuementos()
  {
    return this.http.get(this.url+"/tipodocumento");
  }
}

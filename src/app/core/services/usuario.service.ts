import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private isUserLoggedIn;
  public usserLogged: Usuario;

  constructor() {
    this.isUserLoggedIn = false;
  }

  setUserLoggedIn(user: Usuario) {
//    console.log("User recibio: ", user);
    this.isUserLoggedIn = true;
    this.usserLogged = user;
    sessionStorage.setItem('currentUser', JSON.stringify(user));
    sessionStorage.setItem('username', user.username);
  }

  getUserLoggedIn() {
    return JSON.parse(sessionStorage.getItem('currentUser'));
  }
  getUser() {
    return sessionStorage.getItem('username');
  }
  getRoles() {
    return sessionStorage.getItem('roles');
  }
  getToken(): string {
    return sessionStorage.getItem('tokenHash');
  }
  removeLogged() {
  //  console.log('Logout');
    sessionStorage.removeItem('currentUser');
    sessionStorage.removeItem('nombreUsuario');
    sessionStorage.removeItem('username');
  }

  setData(data: any) {
    sessionStorage.setItem('data', JSON.stringify(data));
  }
  getData() {
    return JSON.parse(sessionStorage.getItem('data'));
  }
  /* logout() {
    try {
      localStorage.removeItem('access_token_app');
      localStorage.removeItem('refresh_token_app');
      localStorage.removeItem('currentUser');
      this.isUserLoggedIn.next(false);
    } catch (err) {
      console.log(err);
    }
  } */
}

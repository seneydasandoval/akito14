import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Servers } from 'src/app/config/api';
import { map } from 'rxjs/operators';
import { UsuarioService } from 'src/app/core/services/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class VisualizarDocumentosService {
  private url = Servers.SIFEN.baseUrl + '/gestionDocumentos';
  private datos ='/datosterceros';
  private listado = '/listadocumento'; 
  private documento = '/documentos/imagenes';
  private eliminar = '/eliminar';
  private agregar = '/subiradjunto';
  private actualizar = '/actualizar';

  constructor(private http: HttpClient,
    private usuarioService: UsuarioService) {}

  getTipoDocumentos()
  {
    return this.http.get(this.url+"/tipodocumento/tomador");
  }

  getDatosTerceros(params){
    return this.http.get(this.url + this.datos +"?"+ params, );
  }

  getListaDocumentos(params){
    return this.http.get(this.url + this.listado +"?"+ params, );
  }

  getDocumentos(params){
    return this.http.get(this.url + this.documento +"?"+ params, );
  }

  deleteDocumento(data) {
    return this.http.post(this.url + this.eliminar,data);
  }


  getSubTipoDocumentos(){
    return this.http.get(this.url+"/tipodocumento");
  }

  postDocumentos( params,imagenParaSubir: File){
    const formData = new FormData();
    formData.append('file', imagenParaSubir);
    let headers= new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');      
    return this.http.post(this.url + this.agregar +'?' + params,formData,{headers: headers,observe: 'response', responseType:'text'}).pipe(map(result=>result.body));
  }

  putDocumento(data) {
    return this.http.post(this.url + this.actualizar,data);
  }

  hasPermissions(){
    let roles = this.usuarioService.getRoles();
    if(roles.indexOf('DOCADM') > -1){
      return true;
    }else{
      return false;
    }
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import {DialogComentarioComponent} from '../../pages/common/dialog-comentario.component';

@Injectable()
export class DialogService {

  constructor(private modalService: NgbModal) { }

  public confirm(
    title: string,
    btnOkText: string = 'Aceptar',
    btnCancelText: string = 'Cancelar',
    dialogSize: 'sm'|'lg' = 'sm'): Promise<boolean> {
    const modalRef = this.modalService.open(DialogComentarioComponent, { size: dialogSize });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnCancelText = btnCancelText;

    return modalRef.result;
  }

  public openAgregarComentario(data, dialogSize: 'lg' = 'lg',) {
    const modalRef = this.modalService.open(DialogComentarioComponent, { size: dialogSize });
    modalRef.componentInstance.data = data;
    return modalRef.result;
  }

  public open(){
    
  }


}

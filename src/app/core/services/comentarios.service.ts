import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Servers } from 'src/app/config/api';
import { Factura } from '../models/factura.model';

@Injectable({
  providedIn: 'root'
})
export class ComentariosService {
  private url = Servers.SIFEN.baseUrl + '/gestionFactura';
  private listadoComentarios = '/listaComentarios';
  private postComentario = '/guardarHistorico';
  private listadoMotivos = '/listaMotivos';
  private listadoTipoGestion = '/listaTipoGestion';

  constructor(private http: HttpClient) {
  //  console.log('Servicio Comentarios');
  }

  getComentarios(params) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
   // console.log("url ccomentario: ",this.url + this.listadoComentarios + params);
    return this.http.get(this.url + this.listadoComentarios + params, {
      headers
    });
  }
  getMotivos() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.listadoMotivos, { headers });
  }
  getTipoGestion() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.listadoTipoGestion, { headers });
  }
  postComentar(data) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
   let urlComentar = this.url + this.postComentario;

  // console.log("DAtos recibidos: ", JSON.parse(data));

   let body = JSON.parse(data);
   
    return this.http.post(urlComentar, body, {headers});
    
   // return this.http.post(this.url + this.postComentario,{ data},{headers});
  }

  //POST comentario
}

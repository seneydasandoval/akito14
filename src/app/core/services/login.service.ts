import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ServersLogin } from 'src/app/config/api';
import { Servers } from 'src/app/config/api';
//import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private authUrl = ServersLogin.SIFENL.baseUrl + '/autenticador/login';
  private url = Servers.SIFEN.baseUrl + '/gestionFactura';

  private postComentario = '/guardarHistorico';
  constructor(private http: HttpClient) {}

  login(username: string, password: string) {
 //   console.log("Entra a login");
  //  console.log(this.authUrl);
    return this.http.post(this.authUrl, {
      username: username,
      password: password
    });
  }

  logout() {}
}

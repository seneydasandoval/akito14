import { TestBed } from '@angular/core/testing';

import { VisualizarDocumentosService } from './visualizar-documentos.service';

describe('VisualizarDocumentosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VisualizarDocumentosService = TestBed.get(VisualizarDocumentosService);
    expect(service).toBeTruthy();
  });
});

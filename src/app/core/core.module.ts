import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Servicios
import { FacturasService, LoginService, ComentariosService } from './services';

// Modulos
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  providers: [
    /*Servicios*/
    FacturasService,
    LoginService
  ]
})
export class CoreModule {}

import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 * value | exponentialStrength:exponent
 * Example:
 * {{ 2 | exponentialStrength:10}}
 * formats to: 1024
 */
@Pipe({ name: 'separadorMiles' })
export class SeparadorMilesPipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      let valueString = `${value}`;
      let res = valueString.replace('.', ',');

      let respuesta2 = res.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
      return respuesta2;
    } else {
      return value;
    }
  }
}

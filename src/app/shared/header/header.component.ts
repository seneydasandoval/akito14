import { Component, OnInit, Input } from '@angular/core';
import { UsuarioService } from '../../core/services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  usuario: string;
  @Input() public nombreUsuario:string;
  @Input() public logueado: boolean;
  constructor(private usuarioService: UsuarioService, private router: Router) {}

  ngOnInit() {
  
    
  }
  logout() {
    this.logueado = false;
    this.usuarioService.removeLogged();
    this.router.navigate(['/']);

  }
  goHome(){
    this.router.navigateByUrl('gestiondocumentos/listarFacturas');
  }
}

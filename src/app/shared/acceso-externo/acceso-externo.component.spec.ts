import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccesoExternoComponent } from './acceso-externo.component';

describe('AccesoExternoComponent', () => {
  let component: AccesoExternoComponent;
  let fixture: ComponentFixture<AccesoExternoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccesoExternoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccesoExternoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from 'src/app/core/services/usuario.service';

@Component({
  selector: 'app-acceso-externo',
  templateUrl: './acceso-externo.component.html',
  styleUrls: ['./acceso-externo.component.css']
})
export class AccesoExternoComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private usuarioService: UsuarioService) { }
  ngOnInit() {
    console.log(this.router.url); //  /routename
    const routePath = this.route.snapshot.paramMap.get('redirectUri');//Se obtiene el id de la ruta 
    console.log(routePath)
    if(routePath != 'undefined'){
      let token = this.usuarioService.getUserLoggedIn();
      console.log(token.tokenHash);
      let token2 = decodeURIComponent(token);
      console.log(token2);
      let tokenBearer = "Bearer " + token2;

    
      }
    }
  }



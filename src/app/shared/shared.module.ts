import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NopageComponent } from './nopage/nopage.component';
import { RouterModule } from '@angular/router';
import { SeparadorMilesPipe } from './pipes/separador-miles.pipe';
import { AccesoExternoComponent } from './acceso-externo/acceso-externo.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    NopageComponent,
    SeparadorMilesPipe,
    AccesoExternoComponent
  ],
  imports: [CommonModule, RouterModule],
  exports: [
    HeaderComponent,
    FooterComponent,
    NopageComponent,
    SeparadorMilesPipe
  ]
})
export class SharedModule {}

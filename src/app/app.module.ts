import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { PagesModule } from './pages/pages.module';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { APP_ROUTES } from './app.routes';

import { AuthInterceptor } from './util/auth.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {Mensajes} from './core/services/mensajes';
import {CoreComponent} from './core/core.component';
import {DialogService} from './core/services/dialog.service';
import {DialogComentarioComponent} from './pages/common/dialog-comentario.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { AgregarDocumentosComponent } from './pages/documentos/agregar-documentos/agregar-documentos.component';
import { EliminarDocumentosComponent } from './pages/documentos/eliminar-documentos/eliminar-documentos.component';
import { EditarDocumentosComponent } from './pages/documentos/editar-documentos/editar-documentos.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
  declarations: [AppComponent,  CoreComponent, DialogComentarioComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    PagesModule,
    SharedModule,
    APP_ROUTES,
    ReactiveFormsModule,
    FlexLayoutModule,
    LayoutModule,
    ToastrModule.forRoot(),
    NgbModule.forRoot(),
    ModalModule.forRoot(),

  ],
  providers: [AuthInterceptor,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
      },
    Mensajes,
    DialogService,
    BsModalService

  ],
  bootstrap: [AppComponent],
  entryComponents:[DialogComentarioComponent, AgregarDocumentosComponent, EliminarDocumentosComponent, EditarDocumentosComponent]
})
export class AppModule {}

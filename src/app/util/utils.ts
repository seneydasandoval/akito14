export function deleteEmptyData(data){
  for(const key in data) {
    if(data[key] == '' || data[key] == null) {
      delete data[key];
    }
  }
  return data;
}

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders, HttpResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {UsuarioService} from '../core/services/usuario.service';
import {catchError, map} from 'rxjs/operators';
import {ErrorObservable} from 'rxjs-compat/observable/ErrorObservable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router:Router, private usuarioService: UsuarioService) {

  }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    if (currentUser && currentUser.tokenHash) {
      let _ = null;
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.tokenHash}`,
          aplicacion: 'MapSifen'
        }
      });
      request.headers.set('Content-Type', 'application/json');
    } else {
      request = request.clone({
        setHeaders: {
          aplicacion: 'MapSifen'
        }
      });
    }

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // TODO Controlar estado de Respuesta
        }
        setTimeout(() => {
        }, 1000);
        return event;
      }),
      catchError((err: any, caught) => {

        if (err.status == 401)
        {
          this.router.navigate(['/']);
          this.usuarioService.removeLogged();
        }
        setTimeout(() => {
        }, 1000);
        return ErrorObservable.create(err);
      })
    );
  }
}

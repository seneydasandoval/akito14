import { Component } from '@angular/core';
import * as $ from 'jquery';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nombreusuario: string;
  logueado: boolean;
  title = 'mapfre-frontend';
  constructor(public router: Router){
    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
   //     console.log("Entra!");
        if(sessionStorage.getItem('nombreUsuario')!= null && sessionStorage.getItem('nombreUsuario')!= undefined && sessionStorage.getItem('nombreUsuario')!= 'undefined'){
          this.logueado = true;
        }
        this.nombreusuario = sessionStorage.getItem('nombreUsuario');
      }
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { deleteEmptyData } from '../../../util/utils';
import { Mensajes } from '../../../core/services/mensajes';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { VisualizarDocumentosService } from 'src/app/core/services/visualizar-documentos.service';
import { AgregarDocumentosComponent } from '../agregar-documentos/agregar-documentos.component';
import { EliminarDocumentosComponent } from '../eliminar-documentos/eliminar-documentos.component';
import { EditarDocumentosComponent } from '../editar-documentos/editar-documentos.component';
declare const $: any;


@Component({
  selector: 'app-visualizar-documentos',
  templateUrl: './visualizar-documentos.component.html',
  styleUrls: ['./visualizar-documentos.component.css']
})
export class VisualizarDocumentosComponent implements OnInit {
  cargando = false;
  /* Parametros para generar la url */
  params = {
    tipoDocumento: '',
    codigoDocumento: '',
    numeroItem: '',
  };
  /* objeto para almacenar datos del tomador */
  tomador = {
    nombre: '',
    codigoDocumento: '',
    apellido: ''
  };
  /* objeto para almacenar datos del archivo para el modal */

  datosObjetoPublico = {
    subTipoDocumento: '',
    nombre: '',
    fechaVecimiento: '',
    importeIngreso: '',
    archivo: '',
    numeroItem:''
  };
  datosObjetoPrivado = {
    subTipoDocumento: '',
    nombre: '',
    fechaVecimiento: '',
    importeIngreso: '',
    archivo: '',
    numeroItem:''
  };
  botonbuscador: boolean = true;
  bsModalRef: BsModalRef;
  filtrosForm: FormGroup;
  tipoDocumentos: any[] = [];
  datosTerceros: any[] = [];
  listadocumentos: any[] = [];
  listadocumentosPublicos: any[] = [];
  listadocumentosPrivados: any[] = [];
  documento: any[] = [];
  objetoPublico: any [] = [];
  objetoPrivado: any[] =[];
  existeTomador: boolean = false;
  existeDocPublico: boolean = false;
  existeDocPrivado: boolean = false;
  objetoSeleccionado;
  indiceSeleccionado;
  
  constructor( 
    private visualizar: VisualizarDocumentosService,
    private mensajeResp: Mensajes,
    private sanitizer:DomSanitizer,
    private bsModalService: BsModalService) 
  { }

  ngOnInit() {
    this.cargando = true;
    this.filtrosForm = new FormGroup({
      tipoDocumento: new FormControl(''),
      codigoDocumento: new FormControl('')
    });
    /**
     * habilita los campos del form de busqueda
     */
    this.filtrosForm.get('tipoDocumento').enable();
    this.filtrosForm.get('codigoDocumento').enable();
    /* URL inicial y primera página*/
    let url = `tipoDocumento=${this.params.tipoDocumento}&codigoDocumento=${this.params.codigoDocumento}`;
    this.obtenerTipoDocumento();

  }

  obtenerTipoDocumento() {
    this.visualizar.getTipoDocumentos().subscribe(
      (resp: any) => {
        if (!resp.error) {
          this.cargando = false;
          if (resp.dato.length > 0) {
            this.tipoDocumentos = resp.dato;
          }
        } else {
          this.cargando = false;
        }
      },
    );
  }

  buscar() {
    this.datosObjetoPublico = {
      subTipoDocumento: '',
      nombre: '',
      fechaVecimiento: '',
      importeIngreso: '',
      archivo: '',
      numeroItem:''
    };
    this.datosObjetoPrivado = {
      subTipoDocumento: '',
      nombre: '',
      fechaVecimiento: '',
      importeIngreso: '',
      archivo: '',
      numeroItem:''
    };
    this.indiceSeleccionado = null;
    this.listadocumentosPublicos= [];
    this.listadocumentosPrivados = [];
    this.documento = [];
    this.cargando = true;
    this.params = this.filtrosForm.value;
    this.existeTomador = false;
    this.objetoPublico = [];
    this.objetoPrivado = [];
    this.botonbuscador = false;
    this.filtrosForm.get('tipoDocumento').disable();
    this.filtrosForm.get('codigoDocumento').disable();
    this.obtenerDatosTerceros(this.params);
  }

  obtenerDatosTerceros(data) {
    let url = `tipoDocumento=${this.params.tipoDocumento}&codigoDocumento=${this.params.codigoDocumento}`;
     this.visualizar.getDatosTerceros(deleteEmptyData(url)).subscribe(
      (res: any) => {
      this.cargando = false;
      if (!res.error) {
        if (res.dato != null) {
          this.datosTerceros = res.dato;
          this.tomador.nombre = res.dato.nombre;
          this.tomador.apellido = res.dato.primerApellido;
          this.tomador.codigoDocumento = res.dato.codigoDocumento;
          this.existeDocPrivado = false;
          this.existeDocPublico = false;
          if(this.tomador.nombre !=null && this.tomador.codigoDocumento !=null){
            this.existeTomador = true;
            this.cargando = true;
            this.obtenerListaDocPublicos(this.params);
            this.obtenerListaDocPrivados(this.params);
            this.filtrosForm.get('tipoDocumento').disable();
            this.filtrosForm.get('codigoDocumento').disable();
          }else if(this.tomador.nombre == null && this.tomador.codigoDocumento == null){
            this.existeTomador = false;
            this.botonbuscador = true;
            this.filtrosForm.get('tipoDocumento').enable();
            this.filtrosForm.get('codigoDocumento').enable();
            this.existeTomador = false;
          }
        } 
        }else{
          this.existeTomador = false;
        }
      },
    ); 
  }


  obtenerListaDocPublicos(data){
    let url = `tipoDocumento=${this.params.tipoDocumento}&codigoDocumento=${this.params.codigoDocumento}`;
      this.visualizar.getListaDocumentos(deleteEmptyData(url)).subscribe(
      (respuesta: any) => {
      this.cargando = false;
      if (!respuesta.error) {
        if (respuesta.dato != null) {
          this.existeDocPublico = true;
          this.listadocumentosPublicos = respuesta.dato.publico;
          console.log("Listado Publico", this.listadocumentosPublicos);
          if (this.listadocumentosPublicos.length == 0){
            this.existeDocPublico = false;
          }
          if(this.listadocumentosPublicos != null){
            let items = this.listadocumentosPublicos;
            let i = 0;
            for (let item of items) {
              this.params.numeroItem = item.numeroItem;
              let aux = {
              nombre: 'assets/img/file.png',
              id: item.numeroItem,
              tipo: item.nombreFichero,
            };
            this.objetoPublico.splice(i,0,aux);
            this.obtenerDocumentoByItemPublico(i);
            i++;
            }
          }
        }else{
          this.existeDocPublico = false;
        } 
      }else{
        this.existeDocPublico = false;
      }  
    });
  }

  
  obtenerListaDocPrivados(data){
    let url = `tipoDocumento=${this.params.tipoDocumento}&codigoDocumento=${this.params.codigoDocumento}`;
    this.visualizar.getListaDocumentos(deleteEmptyData(url)).subscribe(
    (respuesta: any) => {
      this.cargando = false;
      if (!respuesta.error) {
        if (respuesta.dato != null) {
          this.existeDocPrivado = true;
          this.listadocumentosPrivados = respuesta.dato.privado;
          console.log("Listado Privado ", this.listadocumentosPrivados);
          if (this.listadocumentosPrivados.length == 0){
            this.existeDocPrivado = false;
          }
          if(this.listadocumentosPrivados != null){
            let items = this.listadocumentosPrivados;
            let i = 0;
            for (let item of items) {
              let aux = {
                nombre: 'assets/img/file.png',
                id: item.numeroItem,
                tipo: item.nombreFichero,
              };
              this.objetoPrivado.splice(i,0,aux); 
              this.obtenerDocumentoByItemPrivado(i);
              i++;
            }    
          }
        } 
      } else{
        this.existeDocPrivado = true;
      }
    });
  }

  obtenerDocumentoByItemPublico(i){
    let url = `numeroItem=${this.objetoPublico[i].id}&tipoDocumento=${this.params.tipoDocumento}&codigoDocumento=${this.params.codigoDocumento}`;
    /**
     * se deserializa el archivo, si es una imagen
     */
    if (this.objetoPublico[i].tipo.toLowerCase().indexOf('.jpg') > -1 
    || this.objetoPublico[i].tipo.toLowerCase().indexOf('.png')  > -1 
    || this.objetoPublico[i].tipo.toLowerCase().indexOf('.tiff') > -1 
    || this.objetoPublico[i].tipo.toLowerCase().indexOf('.gif')  > -1 
    || this.objetoPublico[i].tipo.toLowerCase().indexOf('.jpeg') > -1){
      this.visualizar.getDocumentos(deleteEmptyData(url)).subscribe(
        async (response: any) => {
        if (response!=null) {
            this.documento = response;
            this.deserializarImagenPublico(this.documento, i);
          } 
        }
      );
    /**
     * asigna una imagen por defecto a los archivos
     */
    }else if(this.objetoPublico[i].tipo.toLowerCase().indexOf('.pdf') > -1){
      this.objetoPublico[i].nombre = 'assets/img/pdf.png';
    }else if(this.objetoPublico[i].tipo.toLowerCase().indexOf('xls') > -1 || this.objetoPublico[i].tipo.toLowerCase().indexOf('.xlsx') > -1  ){
     this.objetoPublico[i].nombre = 'assets/img/xls.png'
    }else if(this.objetoPublico[i].tipo.toLowerCase().indexOf('docx') > -1 || this.objetoPublico[i].tipo.toLowerCase().indexOf('.odt') > -1 || this.objetoPublico[i].tipo.toLowerCase().indexOf('docs') ){
      this.objetoPublico[i].nombre = 'assets/img/doc.png'
    }else{
    this.objetoPublico[i].nombre = 'assets/img/file.png';
    }
  }


  obtenerDocumentoByItemPrivado(i){
    let url = `numeroItem=${this.objetoPrivado[i].id}&tipoDocumento=${this.params.tipoDocumento}&codigoDocumento=${this.params.codigoDocumento}`;
      /**
     * se deserializa el archivo, si es una imagen
     */
    if (this.objetoPrivado[i].tipo.toLowerCase().indexOf('.jpg') > -1 
    || this.objetoPrivado[i].tipo.toLowerCase().indexOf('.png')  > -1 
    || this.objetoPrivado[i].tipo.toLowerCase().indexOf('.tiff') > -1 
    || this.objetoPrivado[i].tipo.toLowerCase().indexOf('.gif')  > -1 
    || this.objetoPrivado[i].tipo.toLowerCase().indexOf('.jpeg') > -1){
      this.visualizar.getDocumentos(deleteEmptyData(url)).subscribe(
        async (response: any) => {
        if (response!=null) {
          this.documento = response;
          this.deserializarImagenPrivado(this.documento,i);
        } 
      });
    /**
     * asigna una imagen por defecto a los archivos
     */
    }else if(this.objetoPrivado[i].tipo.toLowerCase().indexOf('.pdf') > -1){
      this.objetoPrivado[i].nombre = 'assets/img/pdf.png';
    }else if(this.objetoPrivado[i].tipo.toLowerCase().indexOf('xls') > -1 || this.objetoPrivado[i].tipo.toLowerCase().indexOf('.xlsx') > -1  ){
    this.objetoPrivado[i].nombre = 'assets/img/xls.png'
    }else if(this.objetoPrivado[i].tipo.toLowerCase().indexOf('docx') > -1 || this.objetoPrivado[i].tipo.toLowerCase().indexOf('.odt') > -1 || this.objetoPrivado[i].tipo.toLowerCase().indexOf('docs') > -1  ){
      this.objetoPrivado[i].nombre = 'assets/img/doc.png'
    }else{
      this.objetoPrivado[i].nombre = 'assets/img/file.png';
    }
  }

  deserializarImagenPublico(data, i){
    let dato = 'data:image/jpg;base64,' + (this.sanitizer.bypassSecurityTrustResourceUrl(data) as any).changingThisBreaksApplicationSecurity;
    this.objetoPublico[i].nombre =  dato;
  }

  deserializarImagenPrivado(data,i){
   let dato = 'data:image/jpg;base64,' + (this.sanitizer.bypassSecurityTrustResourceUrl(data) as any).changingThisBreaksApplicationSecurity; 
   this.objetoPrivado[i].nombre = dato;
  }


  limpiar() {
    this.datosObjetoPublico = {
      subTipoDocumento: '',
      nombre: '',
      fechaVecimiento: '',
      importeIngreso: '',
      archivo: '',
      numeroItem:''
    };
    this.datosObjetoPrivado = {
      subTipoDocumento: '',
      nombre: '',
      fechaVecimiento: '',
      importeIngreso: '',
      archivo: '',
      numeroItem:''
    };
    this.params = this.filtrosForm.value;
    this.existeDocPrivado = false;
    this.existeDocPublico = false;
    this.objetoPublico = null;
    this.objetoPrivado = null;
    this.existeTomador = false;
    this.botonbuscador = true;
    this.indiceSeleccionado = null;
    this.filtrosForm.get('tipoDocumento').enable();
    this.filtrosForm.get('codigoDocumento').enable();
    this.filtrosForm = new FormGroup({
      tipoDocumento: new FormControl(''),
      codigoDocumento: new FormControl('')
    });
  }

  agregarDocumento() {
    const initialState = {
      tipoDocumento: `${this.params.tipoDocumento}`,
      codigoDocumento: `${this.params.codigoDocumento}`,
    };
    this.bsModalRef = this.bsModalService.show(AgregarDocumentosComponent,{initialState,animated: true,backdrop: 'static',class: 'modal-lg' });
    this.bsModalRef.content.event.subscribe(result => {
      if (result == 'OK') {
        this.buscar();
      }
    });
  }

  editarDocumento(data) {
      $("#image-gallery").modal('hide');
      $("#image-gallery2").modal('hide');
     if (
      data.fechaVecimiento != null &&
      data.fechaVecimiento != '' &&
      data.fechaVecimiento != 'undefined'
    ) {
      let partes = data.fechaVecimiento.split('/');
      let dia = partes[0];
      let mes = partes[1];
      let anho = partes[2];
      data.fechaVecimiento = anho + '-' + mes + '-' + dia;
      console.log(data.fechaVecimiento)
    }
  
    let initialState = {
      img: `${data.archivo}`,
      tipoDocumento: `${this.params.tipoDocumento}`,
      codigoDocumento: `${this.params.codigoDocumento}`,
      numeroItem: `${data.numeroItem}`,
      nombre: `${data.nombre}`,
      importeIngreso:`${data.importeIngreso}`,
      fechaVencimiento:`${data.fechaVecimiento}`,
      subTipoDocumento: `${data.subTipoDocumento}`,
    };
    this.bsModalRef = this.bsModalService.show(EditarDocumentosComponent,{initialState,animated: true,backdrop: 'static',class: 'modal-lg' });
    this.bsModalRef.content.event.subscribe(result => {
      if (result == 'OK') {
        this.buscar();
      }
    });
  }


  borrarDocumento1(data) {
    $("[data-dismiss=modal]").trigger({ type: "click" });
    let initialState = {
      tipoDocumento: `${this.params.tipoDocumento}`,
      codigoDocumento: `${this.params.codigoDocumento}`,
      numeroItem: `${data.numeroItem}`,
      nombre: `${data.nombre}`
    };
    this.bsModalRef = this.bsModalService.show(EliminarDocumentosComponent,{initialState,animated: true,backdrop: 'static'});
    this.bsModalRef.content.event.subscribe(result => {
      if (result == 'OK') 
      {
        this.bsModalRef.hide();
        $('.body').addClass(".fixModal");
        this.buscar();
      }
    });
  }


  abrirDocumento(data, indice){
    this.objetoSeleccionado =  JSON.parse(JSON.stringify(data));
    this.indiceSeleccionado = indice;
      this.datosObjetoPublico = {
        subTipoDocumento: this.listadocumentosPublicos[indice].subTipoDocumento,
        nombre: this.listadocumentosPublicos[indice].nombre,
        fechaVecimiento: this.listadocumentosPublicos[indice].fechaVecimiento,
        importeIngreso: this.listadocumentosPublicos[indice].importeIngreso,
        archivo: this.objetoSeleccionado.nombre,
        numeroItem: this.objetoSeleccionado.id
      };
  }
  siguiente(){
    if(this.indiceSeleccionado == this.objetoPublico.length-1){
      this.abrirDocumento(this.objetoPublico[0],0);
    }else{
      this.abrirDocumento(this.objetoPublico[this.indiceSeleccionado+1],this.indiceSeleccionado+1);
    }
  }
  anterior(){
    if(this.indiceSeleccionado == 0){
      this.abrirDocumento(this.objetoPublico[this.objetoPublico.length-1],this.objetoPublico.length-1);
    }else{
      this.abrirDocumento(this.objetoPublico[this.indiceSeleccionado-1],this.indiceSeleccionado-1);
    }
  }

  abrirDocumento2(data, indice){
    this.objetoSeleccionado =  JSON.parse(JSON.stringify(data));
    this.indiceSeleccionado = indice;
      this.datosObjetoPrivado = {
        subTipoDocumento: this.listadocumentosPrivados[indice].subTipoDocumento,
        nombre: this.listadocumentosPrivados[indice].nombre,
        fechaVecimiento: this.listadocumentosPrivados[indice].fechaVecimiento,
        importeIngreso: this.listadocumentosPrivados[indice].importeIngreso,
        archivo: this.objetoSeleccionado.nombre,
        numeroItem: this.objetoSeleccionado.id

      };
  }
  
  siguiente2(){
    if(this.indiceSeleccionado == this.objetoPrivado.length-1){
      this.abrirDocumento2(this.objetoPrivado[0],0);
    }else{
      this.abrirDocumento2(this.objetoPrivado[this.indiceSeleccionado+1],this.indiceSeleccionado+1);
    }
  }
  anterior2(){
    if(this.indiceSeleccionado == 0){
      this.abrirDocumento2(this.objetoPrivado[this.objetoPrivado.length-1],this.objetoPrivado.length-1);
    }else{
      this.abrirDocumento2(this.objetoPrivado[this.indiceSeleccionado-1],this.indiceSeleccionado-1);
    }
  }
  
  descargar(data) {
  let file;
  let url = `numeroItem=${data.numeroItem}&tipoDocumento=${this.params.tipoDocumento}&codigoDocumento=${this.params.codigoDocumento}`;
    this.visualizar.getDocumentos(deleteEmptyData(url)).subscribe(
    (response: any) => {
      if (response!=null) {
        file = response;
        this.decodeBase64(file, data);
      } 
    });
  }
  
  decodeBase64(file, data){

    const blobData = this.convertBase64ToBlobData(file);
    if (window.navigator && window.navigator.msSaveOrOpenBlob) { //IE
      window.navigator.msSaveOrOpenBlob(blobData, data.nombre);
    } else { // chrome
      const blob = new Blob([blobData], { type: 'application/octet-stream' });
      const url = window.URL.createObjectURL(blob);
      // window.open(url);
      const link = document.createElement('a');
      link.href = url;
      link.download = data.nombre;
      link.click();
    }
  }
  convertBase64ToBlobData(base64Data: string, contentType: string='application/octet-stream', sliceSize=512) {
    const byteCharacters = atob(base64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

}



import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarDocumentosComponent } from './visualizar-documentos.component';

describe('VisualizarDocumentosComponent', () => {
  let component: VisualizarDocumentosComponent;
  let fixture: ComponentFixture<VisualizarDocumentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizarDocumentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl,Validators } from '@angular/forms';
import { VisualizarDocumentosService } from 'src/app/core/services/visualizar-documentos.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Mensajes } from '../../../core/services/mensajes';
import { deleteEmptyData } from 'src/app/util/utils';
import { DomSanitizer } from '@angular/platform-browser';
import { BsDatepickerConfig, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-agregar-documentos',
  templateUrl: './agregar-documentos.component.html',
  styleUrls: ['./agregar-documentos.component.css']
})
export class AgregarDocumentosComponent implements OnInit {
  subirArchivo= false;
  cargando = false;
  privado = false;
  params = {
    tipoDocumento: '',
    codigoDocumento: '',
    nombre: '',
    importeIngreso: '',
    fechaVencimiento: '',
    subTipoDocumento: '',   
  };
  tipoDocumento;
  tipoDocumento1;
  codigoDocumento;
  form: FormGroup;
  subTipoDocumentos: any[] = [];
  inputIngreso: boolean = false;
  inputFecha: boolean = false;
  previewImagen: boolean = false;
  cargandoArchivo: boolean = false;
  nombreArchivo='';
  files: FileList;
  imgsrc='assets/img/upload.png';
  event: EventEmitter<any>=new EventEmitter();

  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  myDateValue: Date;
  
  datePickerConfig: Partial<BsDatepickerConfig>;
  modalRef: BsModalRef;
  constructor(
    private modalService: BsModalService,
    private builder: FormBuilder,
    private servicioAgregar: VisualizarDocumentosService,
    private bsModalRef: BsModalRef,
    private mensajeResp: Mensajes,
    private sanitizer:DomSanitizer,
    ) {
      this.maxDate.setDate(this.maxDate.getDate() + 7);
      this.bsRangeValue = [this.bsValue, this.maxDate];
      this.myDateValue = new Date();

  }


  ngOnInit() {
    this.datePickerConfig = {
      containerClass: 'theme-default',
      showWeekNumbers: false
    };
    this.cargando = true;
    /**
     * Se inicializan los campos del form
     */
    this.form = this.builder.group({
      tipoDocumento: new FormControl('', []),
      codigoDocumento: new FormControl('', []),
      nombre: new FormControl('',  [Validators.required,Validators.pattern('^[a-zA-Z0-9]+$')]),
      importeIngreso: new FormControl('', [Validators.pattern('[/0-9]*')]),
      fechaVencimiento: new FormControl('', []),
      subTipoDocumento: new FormControl('', Validators.required),
      file: new FormControl('')
    });
    this.obtenerSubTipoDocumento();
  }
  onPostFormSubmit(){
    this.cargando = true;
    /**
     * se crea un objeto, y llenan con los datos extraidos del form
     */
    let postData = {
      'tipoDocumento': this.tipoDocumento,
      'codigoDocumento': this.codigoDocumento,
      'nombre': this.form.get('nombre').value,
      'importeIngreso': this.form.get('importeIngreso').value,
      'fechaVencimiento': this.form.get('fechaVencimiento').value,
      'subTipoDocumento': this.form.get('subTipoDocumento').value,
    };
    if (
      postData.fechaVencimiento != null &&
      postData.fechaVencimiento != '' &&
      postData.fechaVencimiento != 'undefined'
    ) {
      let partes = postData.fechaVencimiento.split('-');
      let anho = partes[0];
      let mes = partes[1];
      let dia = partes[2];
      postData.fechaVencimiento = dia + '/' + mes + '/' + anho;
    }
    /**
     * parametro que se envia al servicio 
     */
    let url = `tipoDocumento=${this.tipoDocumento}&codigoDocumento=${this.codigoDocumento}&nombre=${postData.nombre}&importeIngreso=${postData.importeIngreso}&fechaVecimiento=${postData.fechaVencimiento}&subTipoDocumento=${postData.subTipoDocumento}`;
		this.servicioAgregar.postDocumentos(deleteEmptyData(url),this.files[0]).subscribe(
      (res: any) => {
        this.cargando = false;
        if (res == '\"ok\"'){
          this.cargando = false;
          this.mensajeResp.exito("Agregar Documento", "Guardado exitosamente");
          this.event.emit('OK');
          this.bsModalRef.hide();
        }else{
          this.cargando = false;
          this.mensajeResp.warning("Agregar Documento", res);
        }
      }, (error: any) => {
        this.cargando = false;
        this.mensajeResp.warning("Agregar Documento", error.error);
        this.bsModalRef.hide();
      }

    );
  }
  
  obtenerSubTipoDocumento() {
    this.cargando = true;
    this.servicioAgregar.getSubTipoDocumentos().subscribe(
      (resp: any) => {
        if (!resp.error) {
          if (resp.dato.length > 0) {
            this.cargando = false;
            this.subTipoDocumentos = resp.dato;
          }else{
            this.cargando = false;
          }
        }else{
          this.cargando = false;
        }
      },
    );
  }

  habilitarInputs(value: any){
    this.inputFecha = false;
    this.inputIngreso = false;
    let items = this.subTipoDocumentos;
    for(let item of items){
      if(item.TIP_DOCUM == value){
        if(item.MCA_INGRESO == 'S'){
          this.inputIngreso = true;
        } 
        if(item.MCA_VCTO_DOCUMENTO == 'S'){
          this.inputFecha = true;
        }
        if(item.MCA_DOC_PRIVADO == 'S'){
          this.privado = true;
        }else{
          this.privado = false;
        }
      }
    }
  }

  cargandoImagen(files: FileList){
    this.cargando = true;
    this.nombreArchivo = files[0].name;
    this.files = files;
    /** Se genera el preview de la imagen*/
    //console.log(files[0].type.indexOf('image'));
    if(files[0].type.indexOf('image') > -1){
      this.imgsrc = window.URL.createObjectURL(files[0]);
      this.subirArchivo= true;
      this.cargando = false;
    }else{
      this.imgsrc='assets/img/upload2.png';
      this.subirArchivo= true;
      this.cargando =false;

    }
  }
  

  onClose(){
    this.bsModalRef.hide();
  }
}



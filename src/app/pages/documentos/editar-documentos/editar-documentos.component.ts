import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl,Validators } from '@angular/forms';
import { VisualizarDocumentosService } from 'src/app/core/services/visualizar-documentos.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Mensajes } from '../../../core/services/mensajes';
import { deleteEmptyData } from 'src/app/util/utils';
import { DomSanitizer } from '@angular/platform-browser';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-editar-documentos',
  templateUrl: './editar-documentos.component.html',
  styleUrls: ['./editar-documentos.component.css']
})
export class EditarDocumentosComponent implements OnInit {
  cargando = false;
  form: FormGroup;

  tipoDocumento:any;
  codigoDocumento: any;
  numeroItem:any;
  nombre: any;
  importeIngreso:any;
  fechaVencimiento:any;
  subTipoDocumento:any;
  marcaImporte:any;
  marcaFechaVcto:any;
  
  img:any;
  files: FileList;
  imgsrc='assets/img/upload.png';
  nombreSplit;
  subTipoDocumentos: any[] = [];
  inputIngreso:boolean = false;
  inputFecha:boolean = false;
  previewImagen: boolean = false;
  cargandoArchivo: boolean = false;
  nombreArchivo='';
  event: EventEmitter<any>=new EventEmitter();
  constructor(
    private builder: FormBuilder,
    private servicioAgregar: VisualizarDocumentosService,
    private bsModalRef: BsModalRef,
    private mensajeResp: Mensajes,
    private sanitizer:DomSanitizer,
    ) {
  }





  ngOnInit() {
    this.cargando = true;
    this.obtenerSubTipoDocumento();
    this.cargarFormulario();
  }



  cargarFormulario() {
   
    if(this.fechaVencimiento == ''){
     // this.fechaVencimiento = formatDate(new Date(), 'yyyy-MM-dd', 'en');
      this.fechaVencimiento = '';

    }else{
    this.fechaVencimiento = new  Date (this.fechaVencimiento).toDateString();
    this.fechaVencimiento = formatDate(this.fechaVencimiento, 'yyyy-MM-dd', 'en')
    }
    //this.nombreSplit = this.nombre.split('.');
    this.form = this.builder.group({
      tipoDocumento: new FormControl(this.tipoDocumento),
      codigoDocumento: new FormControl(this.codigoDocumento),
      //nombre: new FormControl(this.nombreSplit[0], [Validators.required,Validators.pattern('^[a-zA-Z0-9]+$')]),
      importeIngreso: new FormControl(this.importeIngreso, [Validators.pattern('[/0-9]*')]),
      fechaVencimiento: new FormControl(this.fechaVencimiento),
      subTipoDocumento: new FormControl(this.subTipoDocumento, Validators.required),
    });
    this.cargando = false;
  }


  onPostFormSubmit(){
    this.cargando = true;
    /**
     * se crea un objeto, y llenan con los datos extraidos del form
     */
    let postData = {
      'tipoDocumento': this.tipoDocumento,
      'codigoDocumento': this.codigoDocumento,
      //'nombre': this.form.get('nombre').value,
      'nombre': this.nombre,
      'importeIngreso': this.form.get('importeIngreso').value,
      'fechaVencimiento': this.form.get('fechaVencimiento').value,
      'subTipoDocumento': this.form.get('subTipoDocumento').value,
      'marcaImporte':  this.marcaImporte,
      'marcaFechaVcto': this.marcaFechaVcto
    };
    /**
     * se validan los datos que seran enviados
     */
    if (
      postData.fechaVencimiento != null &&
      postData.fechaVencimiento != '' &&
      postData.fechaVencimiento != 'undefined'
    ) {
      let partes = postData.fechaVencimiento.split('-');
      let anho = partes[0];
      let mes = partes[1];
      let dia = partes[2];
      postData.fechaVencimiento = dia + '/' + mes + '/' + anho;
    }
    /**setea los campos, segun la marca */
    if(postData.marcaImporte == 'N'){
      postData.importeIngreso = '';
    }
    if(postData.marcaFechaVcto == 'N' ){
      postData.fechaVencimiento = '';
    }
    /**
     * parametro que se envia al servicio 
     */
    let params = {
      'tipoDocumento': this.tipoDocumento,
      'codigoDocumento': this.codigoDocumento,
      'numeroItem': this.numeroItem,
      'subTipoDocumento': postData.subTipoDocumento,
      //'nombre': postData.nombre + '.'+this.nombreSplit[1],
      'nombre': this.nombre,
      'importeIngreso': postData.importeIngreso,
      'fechaVecimiento': postData.fechaVencimiento,
      'marcaImporte': postData.marcaImporte,
      'marcaFechaVcto': postData.marcaFechaVcto
    }

 		this.servicioAgregar.putDocumento(deleteEmptyData(params)).subscribe(
      (res: any) => {
        if (!res.error || res == 'ok')
        {
          this.cargando = false;
          this.mensajeResp.exito("Editar Documento", "Guardado exitosamente");
          this.event.emit('OK');
          this.bsModalRef.hide();
        }
        else
        {
          this.cargando = false;
          this.mensajeResp.warning("Editar Documento", res.mensaje);
          this.bsModalRef.hide();
        }
      },(error: any) => {
        this.cargando = false;
        console.log(error)
        this.mensajeResp.warning("Editar Documento", error.mensaje);
        this.bsModalRef.hide();
      }
    ); 
  }
  
  obtenerSubTipoDocumento() {   
    this.cargando = true; 
    this.servicioAgregar.getSubTipoDocumentos().subscribe(
      (resp: any) => {
        if (!resp.error) {
          if (resp.dato.length > 0) {
            this.subTipoDocumentos = resp.dato;
            this.cargando = false; 
            this.verificarMarcas();
          }else{
            this.cargando = false;
          }
        }else{
          this.cargando = false;
        }
      }
    );
  }

  habilitarInputs(value: any){
    this.inputFecha = false;
    this.inputIngreso = false;
    let items = this.subTipoDocumentos;
    for(let item of items){
      if(item.TIP_DOCUM == value){
        if(item.MCA_INGRESO == 'S'){
          this.inputIngreso = true;
          this.marcaImporte = 'S';

        }else{
          this.marcaImporte = 'N';
        }
        if(item.MCA_VCTO_DOCUMENTO == 'S'){
          this.inputFecha = true;
          this.marcaFechaVcto = 'S';
        }else{
          this.marcaFechaVcto = 'N';

        }
      }
    }
  }

  verificarMarcas(){
    this.inputFecha = false;
    this.inputIngreso = false;
    let items = this.subTipoDocumentos;
    for(let item of items){
      if(this.subTipoDocumento == item.TIP_DOCUM){
        if(item.MCA_INGRESO == 'S'){
          this.marcaImporte = 'S';
          this.inputIngreso = true;
        }else{
          this.marcaImporte = 'N';
        }
        if(item.MCA_VCTO_DOCUMENTO == 'S'){
          this.marcaFechaVcto = 'S';
          this.inputFecha = true;
        }else{
          this.marcaFechaVcto = 'N';
        }
      }
    }
  }
  onClose(){
    this.bsModalRef.hide();
  }
}

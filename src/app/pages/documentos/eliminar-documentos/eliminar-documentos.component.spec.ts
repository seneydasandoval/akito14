import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarDocumentosComponent } from './eliminar-documentos.component';

describe('EliminarDocumentosComponent', () => {
  let component: EliminarDocumentosComponent;
  let fixture: ComponentFixture<EliminarDocumentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarDocumentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

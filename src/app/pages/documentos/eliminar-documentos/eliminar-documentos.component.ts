import { Component, OnInit, EventEmitter,Input,ViewChild, HostListener, ElementRef, Output } from '@angular/core';
import { VisualizarDocumentosService } from 'src/app/core/services/visualizar-documentos.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Mensajes } from '../../../core/services/mensajes';

@Component({
  selector: 'app-eliminar-documentos',
  templateUrl: './eliminar-documentos.component.html',
  styleUrls: ['./eliminar-documentos.component.css']
})

export class EliminarDocumentosComponent implements OnInit {
  cargando = false;
  event: EventEmitter<any>=new EventEmitter();
  tipoDocumento;
  codigoDocumento;
  numeroItem;
  nombre;

  constructor(
    private servicioEliminar: VisualizarDocumentosService,
    private bsModalRef: BsModalRef,
    private mensajeResp: Mensajes) {
  }
  
  
  ngOnInit() {
  }
  deleteDocumento() {
    this.cargando = true;
    let params = {
      'tipoDocumento': this.tipoDocumento,
      'codigoDocumento': this.codigoDocumento,
      'numeroItem': this.numeroItem
    }

    this.servicioEliminar.deleteDocumento(params).subscribe(
      (respuesta: any) => {
      this.cargando = false;
      if (!respuesta.error) {
        if (respuesta.dato != null) {
          this.cargando = false;
          this.event.emit('OK');
          this.bsModalRef.hide();  
          this.mensajeResp.exito(
            'Eliminar Documentos',
            respuesta.mensaje
          );

        }else{
          this.cargando = false;
          this.mensajeResp.warning(
            'Eliminar Documentos',
            respuesta.mensaje
          );  
        } 
      }else{
        this.cargando = false;
        this.mensajeResp.error(
          'Eliminar Documentos',
          respuesta.mensaje
        );  
      }  
    });

  }

  onClose() {
    this.bsModalRef.hide();
  }
}

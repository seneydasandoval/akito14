import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { ListarFacturasComponent } from './facturas/listar-facturas/listar-facturas.component';
import { LoginComponent } from './login/login.component';
import { VerFacturaComponent } from './facturas/ver-factura/ver-factura.component';
import { VisualizarDocumentosComponent } from './documentos/visualizar-documentos/visualizar-documentos.component';
import { AccesoExternoComponent } from '../shared/acceso-externo/acceso-externo.component';

const pagesRoutes: Routes = [
  {
    path: '',
    component: VisualizarDocumentosComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: '', component: LoginComponent },
      { path: 'logout', component: LoginComponent }
    ]
  },
  {
    path:'sifen',
    children: [
      { path: 'listarFacturas', component: HomeComponent },
      { path: 'facturas', component: ListarFacturasComponent },
      { path: 'facturaDetalle/:numeroDocumento', component: VerFacturaComponent}
    ]
  },
  {
    path:'gestiondocumentos',
    children: [
      { path: 'listardocumentos', component: VisualizarDocumentosComponent}
    ]
  },
  { path:'acceso-externo/:redirectUri', component: AccesoExternoComponent},
  { path:'acceso-externos/:id', component: AccesoExternoComponent},
  { path:'acceso-externo', component: AccesoExternoComponent}

];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);

import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup} from '@angular/forms';
import {Mensajes} from '../../core/services/mensajes';
import {UsuarioService} from '../../core/services/usuario.service';
import {ComentariosService} from '../../core/services';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog-comentario.component.html',
})
export class DialogComentarioComponent implements OnInit {

  @Input() data: any;
  @Input() btnOkText: string;
  @Input() btnCancelText: string;
  paramsComentario = {
    tipoGestion: '',
    tipoCorreccion: '',
    comentario: ''
  };
  hoy: string;
  comentarioForm: FormGroup;
  tipoCorreciones: any[] = [];
  tipoGestiones:any[]=[];
  cargando:boolean=false;

  constructor(private activeModal: NgbActiveModal, private mensajeResp: Mensajes, private usuarioService: UsuarioService,private comentariosService: ComentariosService) { }

  ngOnInit() {
    this.comentarioForm = new FormGroup({
      tipoDocumento: new FormControl(''),
      numeroDocumento: new FormControl(''),
      tipoDocumentoCliente: new FormControl(''),
      codigoDocumentoCliente: new FormControl(''),
      fechaEmisionFactura: new FormControl(''),
      codigoTipoCorreccion: new FormControl(''),
      fechaUltimaActualizacion: new FormControl(''),
      codigoUsuario: new FormControl(''),
      observacion: new FormControl(''),
      codigoEstado: new FormControl('')
    });
    this.obtenerFecha();
    this.obtenerMotivos();
    this.obtenerTipoGestion();
  }

  public decline() {
    this.activeModal.close(false);
  }

  public accept() {
    this.activeModal.close(true);
  }

  public dismiss() {
    this.activeModal.dismiss();
  }
  public onSubmitComentario() {
    this.cargando = true;

    /*  Rellenar los otros campos del formulario */

    this.comentarioForm.patchValue({
      tipoDocumento: this.data.tipoDocumento,
      numeroDocumento:  this.data.numeroDocumento,
      tipoDocumentoCliente:  this.data.tipoDocumentoCliente,
      codigoDocumentoCliente: `${ this.data.codigoDocumentoCliente}`,
      fechaEmisionFactura:  this.data.fechaEmisionFactura,
      fechaUltimaActualizacion: this.hoy,
      codigoUsuario: this.usuarioService.getUser()
    });

    this.paramsComentario = this.comentarioForm.value;
    /* Generar url para el post */

    var body2 = JSON.stringify(this.paramsComentario);

    this.comentariosService.postComentar(body2).subscribe(
      (res: any) => {
        this.cargando = false;
        if (!res.error)
        {
          console.log("Exitoso");
          this.mensajeResp.exito("Agregar comentario", res.mensaje);
        }
        else
        {
          this.mensajeResp.error("Agregar comentario", res.mensaje);
        }
        this.dismiss();
      }, error => {
        this.cargando = false;
        this.mensajeResp.error("Agregar comentario", error);
        this.dismiss();
      }
    );
  }
  public obtenerFecha()
  {
    let today: any = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1; //January is 0!

    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    this.hoy = `${dd}/${mm}/${yyyy}`;
  }
  public obtenerMotivos()
  {
    this.comentariosService.getMotivos().subscribe((res: any) => {
      this.tipoCorreciones = res.dato;
      console.log("Res tipo correccion: ", this.tipoCorreciones)
    });
  }
  public obtenerTipoGestion()
  {
    this.comentariosService.getTipoGestion().subscribe((res: any) => {

      this.tipoGestiones = res.dato;
      //    console.log('GEstiones', this.tipoGestion);
    });
  }
  get f() { return this.comentarioForm.controls; }
}

//Modulos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { MaterialModule } from './material.module';
import { BsDatepickerModule } from 'ngx-bootstrap';

// Routes
import { PAGES_ROUTES } from './pages.routes';
import { HomeComponent } from './home/home.component';
import { PagesComponent } from './pages.component';
import { ListarFacturasComponent } from './facturas/listar-facturas/listar-facturas.component';
import { LoginComponent } from './login/login.component';
import { VerFacturaComponent } from './facturas/ver-factura/ver-factura.component';
import { VisualizarDocumentosComponent } from './documentos/visualizar-documentos/visualizar-documentos.component';
import { AgregarDocumentosComponent } from './documentos/agregar-documentos/agregar-documentos.component';
import { EliminarDocumentosComponent } from './documentos/eliminar-documentos/eliminar-documentos.component';
import { EditarDocumentosComponent } from './documentos/editar-documentos/editar-documentos.component';
@NgModule({
  declarations: [
    PagesComponent,
    HomeComponent,
    ListarFacturasComponent,
    LoginComponent,
    VerFacturaComponent,
    VisualizarDocumentosComponent,
    AgregarDocumentosComponent,
    EliminarDocumentosComponent,
    EditarDocumentosComponent,
    ],
  imports: [
    CommonModule,
    MaterialModule,
    PAGES_ROUTES,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BsDatepickerModule.forRoot()

  ],
  exports: [PagesComponent, HomeComponent, RouterModule],
})
export class PagesModule {}

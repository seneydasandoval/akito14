import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { FacturaDetalle, Comentario } from '../../../core/models/factura.model';
import { FacturasService } from '../../../core/services/facturas.service';
import { UsuarioService } from '../../../core/services/usuario.service';
import { ComentariosService } from '../../../core/services/comentarios.service';

import { MatTableDataSource, MatSort, MatCard } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ver-factura',
  templateUrl: './ver-factura.component.html',
  styleUrls: ['./ver-factura.component.css']
})
export class VerFacturaComponent implements OnInit {
  factura: FacturaDetalle;
  data: any;
  comentarios: Comentario[];
  urlDetalle: string;
  cargando = false;
  dataSource: any;
  displayedColumns: string[] = [
    'codigoUsuario',
    'observacion',
    'codigoTipoCorreccion'
  ];
  constructor(
    private activatedRoute: ActivatedRoute,
    private facturasServices: FacturasService,
    private comentariosService: ComentariosService,
    private usuarioService: UsuarioService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.cargando = true;
    this.activatedRoute.params.subscribe(params => {
      /* Generamos la url, extrayendo los parametros de params */
      let data = this.usuarioService.getData();
      this.urlDetalle = `?tipoDocumento=${data.tipoDocumento}&numeroDocumento=${params['numeroDocumento']}&fechaEmisionFactura=${data.fechaEmisionFactura}`;
  //    console.log('URL detalle', this.urlDetalle);

      /* Llamamos al servicio  de Factura detalle*/
      this.facturasServices
        .getFactura(this.urlDetalle)
        .subscribe((res: any) => {
          this.factura = res.dato[0];
 //         console.log('Detalles de la factura', this.factura);

          /* Llamamos al servicio  de comentarios correspondientes a la factura*/
          this.comentariosService
            .getComentarios(this.urlDetalle)
            .subscribe((results: any) => {
              this.cargando = false;
  //            console.log("Comentarios: ", results);
              this.comentarios = results.dato;
              this.dataSource = new MatTableDataSource<Comentario>(
                this.comentarios
              );
    //          console.log(this.comentarios);
            });
        });
    });
  }

  volver(){
    this.router.navigateByUrl('/listarFacturas');
  }
}

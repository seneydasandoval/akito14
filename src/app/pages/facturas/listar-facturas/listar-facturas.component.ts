import { Component, ElementRef, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

/* Servicios */
import { Factura } from '../../../core/models/factura.model';
import { FacturasService } from '../../../core/services/facturas.service';
import { ComentariosService } from '../../../core/services/comentarios.service';
import { UsuarioService } from '../../../core/services/usuario.service';

/* Angular Material */
import { MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';

import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { deleteEmptyData } from '../../../util/utils';
import { Mensajes } from '../../../core/services/mensajes';
import { DialogService } from '../../../core/services/dialog.service';

@Component({
  selector: 'app-listar-facturas',
  templateUrl: './listar-facturas.component.html',
  styleUrls: ['./listar-facturas.component.css']
})
export class ListarFacturasComponent implements OnInit {
  /* Formulario para los filtros */
  filtrosForm: FormGroup;

  /* Formulario para los comentarios */

  /* Listade facturas a mostrar, tipo de gestión y corrección para 
  desplegar en los filtros y modal de comentarios */

  facturas: Factura[] = [];
  tipoGestiones: any[] = [];
  tipoDocumentos: any[] = [];
  tipoCorreccion: any[] = [];
  data: any = {};

  datosComentarios: any = {};

  cargando = false;

  totalItems: number;

  /* Variables para generar la tabla y paginar */
  dataSource: any;
  displayedColumns: string[] = [
    'tipoDocumento',
    'numeroDocumento',
    'tipoDocumentoCliente',
    'codigoDocumentoCliente',
    'nombreCliente',
    'fechaEmisionFactura',
    'fechaEnvioSifen',
    'fechaFirma',
    'moneda',
    'importeTotal',
    'motivoRechazo',
    'action'
  ];

  length = 0;
  itemsPorPagina = 5;
  pageSize = 5;
  pagina = 1;
  pageIndex = 0;

  hayMensaje = false;
  mensaje = '';
  /* Parametros para generar la url */
  params = {
    fechaDesde: '',
    fechaHasta: '',
    numeroPoliza: '',
    numeroDocumento: '',
    tipoDocumentoCliente: '',
    codigoDocumentoCliente: '',
    numeroPagina: '',
    tipoGestion: ''
  };

  URLactual: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private facturasServices: FacturasService,
    private router: Router,
    private tipoGestionService: ComentariosService,
    private usuarioService: UsuarioService,
    private comentariosService: ComentariosService,
    private mensajeResp: Mensajes,
    private dialogService: DialogService
  ) {
    /* Obtener fecha de hoy */
    //   console.log(this.hoy, 'HOY');
  }

  ngOnInit() {
    this.cargando = true;
    /*  Filtros */
    this.filtrosForm = new FormGroup({
      fechaDesde: new FormControl(''),
      fechaHasta: new FormControl(''),
      numeroPoliza: new FormControl(''),
      numeroDocumento: new FormControl(''),
      tipoDocumentoCliente: new FormControl(''),
      codigoDocumentoCliente: new FormControl(''),
      numeroPagina: new FormControl('1'),
      tipoGestion: new FormControl('')
    });
    //    console.log(this.filtrosForm.value, 'Somos los filtros');

    /* Traducción del paginador */
    this.paginator._intl.itemsPerPageLabel = 'Facturas por página  :';

    /* URL inicial y primera página*/
    let url = `fechaDesde=${this.params.fechaDesde}&fechaHasta=${this.params.fechaHasta}&numeroPoliza=${this.params.numeroPoliza}&numeroFactura=${this.params.numeroDocumento}&tipoDocumento=${this.params.tipoDocumentoCliente}&numeroDocumento=${this.params.numeroDocumento}&numeroPagina=${this.pagina}&codigoEstado=${this.params.tipoGestion}`;

    //   console.log("Url para pagina 1: ",url);
    this.paginarFacturas(url);

    /* Gestiones a seleccionar en los filtros */
    this.tipoGestionService.getTipoGestion().subscribe((res: any) => {
      this.tipoGestiones = res.dato;
      //    console.log('GEstiones', this.tipoGestion);
    });
    this.obtenerTipoDocumento();
  }

  callPagination($event) {
    /*  Método que llama al servicio nuevamente 
       con los nuevos parametros de paginación */
    this.data = {
      totalItem: $event.pageSize,
      pageIndex: $event.pageIndex + 1
    };
    this.pagina = this.data.pageIndex;
    this.listadoFacturas();
  }

  listadoFacturas() {
    /* Método que genera la url para el servicio y lo llama */

    let url = `fechaDesde=${this.params.fechaDesde}&fechaHasta=${this.params.fechaHasta}&numeroPoliza=${this.params.numeroPoliza}&numeroFactura=${this.params.numeroDocumento}&tipoDocumento=${this.params.tipoDocumentoCliente}&numeroDocumento=${this.params.numeroDocumento}&numeroPagina=${this.data.pageIndex}&codigoEstado=${this.params.tipoGestion}`;
    console.log(url, 'SOY LA URL DEL PAG');
    this.paginarFacturas(url);
  }
  listadoFacturas2(data) {
    if (
      this.params.fechaDesde != null &&
      this.params.fechaDesde != '' &&
      this.params.fechaDesde != 'undefined'
    ) {
      let partes = this.params.fechaDesde.split('-');
      let anho = partes[0];
      let mes = partes[1];
      let dia = partes[2];

      this.params.fechaDesde = dia + '/' + mes + '/' + anho;
    }

    if (
      this.params.fechaHasta != null &&
      this.params.fechaHasta != '' &&
      this.params.fechaHasta != 'undefined'
    ) {
      let partes = this.params.fechaHasta.split('-');
      let anho = partes[0];
      let mes = partes[1];
      let dia = partes[2];

      this.params.fechaHasta = dia + '/' + mes + '/' + anho;
    }
    /* Método que genera la url para el servicio y lo llama */

    let url = `fechaDesde=${this.params.fechaDesde}&fechaHasta=${this.params.fechaHasta}&numeroPoliza=${this.params.numeroPoliza}&numeroFactura=${this.params.numeroDocumento}&tipoDocumento=${this.params.tipoDocumentoCliente}&numeroDocumento=${this.params.codigoDocumentoCliente}&numeroPagina=${this.params.numeroPagina}&codigoEstado=${this.params.tipoGestion}`;
    console.log(url, 'SOY LA URL DEL PAG');
    this.paginarFacturas(url);
  }

  paginarFacturas(url) {
    /* Método que solicita la página correspondiente, según la tabla de Angular Material */
    this.cargando = true;
    this.facturas = [];
    this.length = 0;
    this.itemsPorPagina = 10;
    this.pageSize = 10;
    this.pagina = 1;
    this.pageIndex = 0;
    this.facturasServices.getFacturas(deleteEmptyData(url)).subscribe(
      (res: any) => {
        this.cargando = false;
        if (!res.error) {
          if (res.dato != null && res.dato.lista.length > 0) {
            this.facturas = res.dato.lista;
            var totalItems = res.dato.totalDatos; //result.itemsTotal;
            this.length = totalItems;
            this.itemsPorPagina = 10; //result.itemsPorPagina;
            this.pageSize = 10; //result.itemsPorPagina;
            this.pagina = res.dato.paginado; //res.dato.pagina;
            this.pageIndex = this.pagina - 1;
            this.dataSource = new MatTableDataSource<Factura>(this.facturas);
          } else {
            this.dataSource = new MatTableDataSource<Factura>(this.facturas);
            this.mensajeResp.warning(
              'Listado de Facturas ',
              'No se encontraron resultados'
            );
          }
        } else {
          this.mensajeResp.error('Listado de Facturas ', res.mensaje);
        }
      },
      error => {
        this.cargando = false;
        this.mensajeResp.error('Listado de Facturas ', error);
      }
    );
  }

  verFacturaDetalle(
    tipoDocumento: string,
    numeroDocumento: string,
    fechaEmisionFactura: string
  ) {
    /* Método que  recibe los parametros necesarios para 
    navegar a los detalles de la factura seleccionada */
    let data = {
      tipoDocumento,
      numeroDocumento,
      fechaEmisionFactura
    };
    this.usuarioService.setData(data);
    this.router.navigate(['/facturaDetalle', numeroDocumento]);
  }

  buscar() {
    this.cargando = true;

    this.params = this.filtrosForm.value;
    this.params.numeroPagina = '1';
    this.listadoFacturas2(this.params);
  }

  limpiar() {
    this.filtrosForm = new FormGroup({
      fechaDesde: new FormControl(''),
      fechaHasta: new FormControl(''),
      numeroPoliza: new FormControl(''),
      numeroDocumento: new FormControl(''),
      tipoDocumentoCliente: new FormControl(''),
      codigoDocumentoCliente: new FormControl(''),
      tipoGestion: new FormControl('')
    });
    this.params = this.filtrosForm.value;
    this.params.numeroPagina = '1';
    this.listadoFacturas2(this.params);
  }

  abrirModal(data) {
    console.log('Los datos enviados son ', data);
    const modalRef = this.dialogService.openAgregarComentario(data);
    modalRef.then(
      (data: any) => {},
      (reason: any) => {
        if (reason == 'cerrar') {
          this.limpiar();
        }
      }
    );
  }
  parametrosComentarioBody(
    tipoDocumento,
    numeroDocumento,
    tipoDocumentoCliente,
    codigoDocumentoCliente,
    fechaEmisionFactura,
    //codigoTipoCorreccion,
    fechaUltimaActualizacion,
    nombreCliente
    //observacion,
    //codigoEstado
  ) {
    //    console.log(this.datosComentarios);
  }

  cerrarMensaje() {
    this.hayMensaje = false;
  }
  obtenerTipoDocumento() {
    this.facturasServices.getTipoDocuementos().subscribe(
      (resp: any) => {
        if (!resp.error) {
          if (resp.dato.length > 0) {
            this.tipoDocumentos = resp.dato;
          }
        } else {
          this.mensajeResp.error(
            'Listado de Tipo Documento ',
            'No se encontraron resultados'
          );
        }
      },
      error => {
        this.mensajeResp.error(
          'Listado de Tipo Documento ',
          'No se encontraron resultados'
        );
      }
    );
  }
}

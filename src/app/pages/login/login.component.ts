import { Component, OnInit } from '@angular/core';
import { MatInput, MatCard, MatFormField } from '@angular/material';
import { LoginService } from '../../core/services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../../core/services/usuario.service';
import { Usuario } from '../../core/models/usuario.model';

export interface User {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user:any;
  version: string = '1.0';
  hayMensaje = false;
  mensaje: string;
  constructor(
    private loginService: LoginService,
    private router: Router,
    private route: ActivatedRoute,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit() {
    this.version = '1.1';
  }


  logIn(username: string, password: string, event: Event) {
    event.preventDefault(); // Avoid default action for the submit button of the login form

    this.mensaje = "Comprobando...";
    this.hayMensaje = true;

    // Calls service to login user to the api rest
    this.user=this.loginService.login(username, password).subscribe(
      (res: any) => {
  //      console.log("Respuesta login: ",res);
        if(res.error){
          this.mensaje = res.mensaje;
          this.hayMensaje = true;
        }else{
          this.hayMensaje = false;
          sessionStorage.setItem('nombreUsuario',res.dato.nombreUsuario);
          sessionStorage.setItem('roles',res.dato.roles);
          let tok = res.dato.token;
          let roles = res.dato.roles;
          let u: Usuario = { username: username, tokenHash: tok,  roles: roles};
  
          this.usuarioService.setUserLoggedIn(u);
        }
       
      },
      error => {
        this.mensaje = error.error.Message;
      },

      () => this.navigate()
    );
  }

  navigate() {
    this.router.navigateByUrl('gestiondocumentos/listardocumentos');
    
  }
}
